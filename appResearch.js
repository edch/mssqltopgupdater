var indexArr = ["COMPOSITE", "IDX30", "Investor33", "MINING", "MANUFACTUR", "PEFINDO25" ,"FINANCE", "KOMPAS100", "SRI-KEHATI", "DBX", "MISC-IND", "SMinfra18", "INFRASTRUC", "INFOBANK15", "TRADE", "AGRI", "JII", "BASIC-IND", "CONSUMER", "MNC36", "PROPERTY", "LQ45", "ISSI", "MBX", "BISNIS-27"];

var pgPool = require('./db.js');
pgPool.init();
setTimeout(function () {}, 2000);

var Promise = require('bluebird');
var moment = require('moment');
var redis = require('redis'); 
const fs = require('fs');
const { promisify } = require('util');
const writeFile = promisify(fs.writeFile);
//const readFileAsync = promisify(fs.readdir); //<-- nodejs module

var schedule = require('node-schedule');

//var sanitizeHtml = require('sanitize-html');
//var jsesc = require('jsesc');
//var fsx = require('fs-extra');

client = redis.createClient(); //Select default database (0)

Promise.promisifyAll(redis.RedisClient.prototype);

var pgcnn;

function delay(time) {
	console.log("Delaying...");
	return new Promise(resolve => {
		setTimeout(resolve,time);
	});
}

/*REDIS CONF*/
client.on("error", function (err) {
    console.log("Redis Error: " + err);
});

/*MS SQL CONF*/
const MSSQLCfg = {
    user: 'sa',
    password: 'emptykosong',
	server: '172.16.19.15',
	//server: '172.31.2.151',
	database: 'MARKET_INFO',
	connectionTimeout : 60000,
 
    options: {
        encrypt: false
    }
}

const sql = require('mssql')
sql.Promise = Promise;

var mssqlPool;

/*
const mssqlPool = new sql.ConnectionPool(MSSQLCfg, err => {
	if (err)
		console.log(err);
	console.log("MSSQL Pool created");
});

mssqlPool.on('error', err => {
	console.log(err);	
});
*/

/*PG SQL CONF*/
var oneSec = 1000;
var oneMin = 60 * oneSec;
var fiveMin = 5 * oneMin;
var fifteenMin = 15 * oneMin;
var halfHour = 30 * oneMin;
var oneHour = 60 * oneMin;
var oneDay = 1440 * oneMin;
var sessTimeout = 36000; //10 hour-expiry period

//STD functions, logging and date formatting for logging
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}

function formatDateForPG(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();

  hours = hours < 10 ? '0' +hours : hours;
  minutes = minutes < 10 ? '0' +minutes : minutes;
  seconds = seconds < 10 ? '0' +seconds : seconds;

  var strTime = hours + ':' + minutes +':' + seconds;
  var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate() + " " + strTime;
}

function formatDateOnlyForPG(date) {  
  var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate();
}

function GetDateOnly(date) {
	var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate();
}

var winston = require('winston');
var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
            'timestamp':function () {
                return formatDate(new Date());
            }, 
            'colorize':true
        })
      //,new (winston.transports.File)({ filename: 'somefile.log' }) 
    ]
});

async function doGetResearchData()
{
	logger.log('info', 'Querying research data...');
	
	var query = ''; //Need only insert the most recent which does not existed yet

	var alreadyExistedID = -1;

	return pgPool.doQuery("select id from research ORDER BY id DESC LIMIT 1").then(function (result) {
		if (result.rowCount > 0)
			alreadyExistedID = result.rows[0].id;
		else
			alreadyExistedID = 0;

		console.log(alreadyExistedID);		
			
		return sql.connect(MSSQLCfg).then(() => {						
			var request = new sql.Request();
			
			return request.query('select * from research where ID > ' +alreadyExistedID);
			//return pool.query('select * from research where ID > ' +alreadyExistedID);
		})
		.then(result => {
			//mssqlPool.close();
			console.log("After fetching data from SQL Server");			
			
			if (result.rowsAffected[0] > 0)
			{
				console.log("Updating to PG");
				var dt, content, promiseArr = [];
				result.recordset.forEach(async function (row) {
					var fileName = row.FILE_NAME.toString().split("|");
					//var tmp = sanitizeHtml(row.RESEARCH_CONTENT, {
					var tmp = row.RESEARCH_CONTENT;
					
					var dt = row.NEWS_DATE;
					dt = GetDateOnly(dt);
					
					var notes = row.NOTES;
					
					tmp = tmp.replace(/'/g, "''");
					notes = notes.replace(/'/g, "''");
					notes = notes.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
					notes = notes.replace(/\n/g, "<br />");

					fileName[1] = fileName[1].replace(/'/g, "''");
					fileName[1] = fileName[1] +".pdf";
					
					//Write pdf file temporarily
					fileName[1] = "../pdf/" +fileName[1];
					//logger.log("info", "Processing " +fileName[1]);			    	
					
					//Kemungkinan gagal disini, tapi kenapa kebanyakan newsnya yang gaagl?
					/*
					fsx.outputFile(fileName[1], row.FILE, "binary").then(() => {
						logger.log("info", fileName[1] +" was saved!");
					});
					*/
					try
					{
						logger.log("info", "Writing file " +fileName[1]);
						fs.writeFileSync(fileName[1], row.FILE);
					}					
					catch(err)
					{
						console.log('Error in writing ' +fileName[1]);
						console.log(err);
					}

					logger.log("info", fileName[1] +" was saved!");

					//Only get the difference
					query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','" +notes +"','" +tmp +"','" +dt +"')";
					promiseArr.push(pgPool.doQuery(query));
				});
				
				return Promise.all(promiseArr);
			}
			else {
				console.log("No data found.. Bailing out");
				Promise.resolve(-1);
			}
		})
		.then(() => {
			console.log('Closing SQL Conn');
			sql.close();
			Promise.resolve(1);
		})
		.catch( err => {
			sql.close();
			console.log(err);
			Promise.resolve(-1);
		});
	});
}


function exitHandler(options, err) {
	
	if (pgPool)
		pgPool.cleanUp();

    if (options.cleanup) logger.log('info', 'exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
	logger.log('info', 'Starting job scheduler (2)...');	

	//Setiap hari jam 6 pagi sampai 18 sore, setiap menit ke 50
	//masih ada bug berkaitan dengan path penyimpanan pdf?	
    //var jobResearch = schedule.scheduleJob('50 6-18 * * 1-5', async function()
    var jobResearch = schedule.scheduleJob('*/15 6-18 * * 1-5', async function() //jadi cek tiap 15 menit?
    {		
		var dt = new Date();
    	console.log("Current time is : " +dt);
		logger.log('info', 'Getting Research Data...');

		try
		{
			await doGetResearchData();
		}
		catch (err)
		{
			console.log(err);
			logger.log('error', 'Error in getting research data');
		}
	});	4
}
else
{
	if (process.argv[2] == 'research')
	{
		logger.log('info', 'Performing direct Research Data update...');
		(async () => {
			await doGetResearchData();
			process.exit(0);
		})();
	}	
	else
	{
		logger.error('Invalid Argument!');
	}

	if (mssqlPool)
		return mssqlPool.close().then(() => console.log("Closing connection..."));
	else return;
}

//Clean up and exit handler
process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));