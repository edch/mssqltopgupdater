var indexArr = ["COMPOSITE", "IDX30", "Investor33", "MINING", "MANUFACTUR", "PEFINDO25" ,"FINANCE", "KOMPAS100", "SRI-KEHATI", "DBX", "MISC-IND", "SMinfra18", "INFRASTRUC", "INFOBANK15", "TRADE", "AGRI", "JII", "BASIC-IND", "CONSUMER", "MNC36", "PROPERTY", "LQ45", "ISSI", "MBX", "BISNIS-27"];

var pgPool = require('./db.js');
//setTimeout(function () {}, 2000);
pgPool.init();
setTimeout(function () {}, 2000);
/*
.then(() => {
	console.log("Finished initializing PG");
});
*/

var schedule = require('node-schedule');
var sanitizeHtml = require('sanitize-html');
var fsx = require('fs-extra');
var jsesc = require('jsesc');

var Promise = require('bluebird');
var moment = require('moment');

var redis = require('redis'); 
client = redis.createClient(); //Select default database (0)

Promise.promisifyAll(redis.RedisClient.prototype);

var pgcnn;

/*REDIS CONF*/
client.on("error", function (err) {
    console.log("Redis Error: " + err);
});

/*MS SQL CONF*/
const MSSQLCfg = {
    user: 'sa',
    password: 'emptykosong',
    server: '172.16.18.5', // You can use 'localhost\\instance' to connect to named instance 
    database: 'MARKET_INFO',
 
    options: {
        encrypt: false
    }
}

const sql = require('mssql')
sql.Promise = Promise;

/*
const mssqlPool = new sql.ConnectionPool(MSSQLCfg, err => {
	if (err)
		console.log(err);
});

mssqlPool.on('error', err => {
    console.log(err);	
    mssqlPool.close();
});
*/

/*PG SQL CONF*/
var oneSec = 1000;
var oneMin = 60 * oneSec;
var fiveMin = 5 * oneMin;
var fifteenMin = 15 * oneMin;
var halfHour = 30 * oneMin;
var oneHour = 60 * oneMin;
var oneDay = 1440 * oneMin;
var sessTimeout = 36000; //10 hour-expiry period

//STD functions, logging and date formatting for logging
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}

function formatDateForPG(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var seconds = date.getSeconds();

  hours = hours < 10 ? '0' +hours : hours;
  minutes = minutes < 10 ? '0' +minutes : minutes;
  seconds = seconds < 10 ? '0' +seconds : seconds;

  var strTime = hours + ':' + minutes +':' + seconds;
  var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate() + " " + strTime;
}

function formatDateOnlyForPG(date) {  
  var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate();
}

function GetDateOnly(date) {
	var dtMonth = date.getMonth()+1;
  return date.getFullYear() + "-" +dtMonth + "-" + date.getDate();
}

var winston = require('winston');
var logger = new (winston.Logger)({
    transports: [
      new (winston.transports.Console)({
            'timestamp':function () {                
                return formatDate(new Date());                
            }, 
            'colorize':true
        })
      //,new (winston.transports.File)({ filename: 'somefile.log' }) 
    ]
});

function delay(time) {
	console.log("Delaying...");
	return new Promise(resolve => {
		setTimeout(resolve,time);
	});
}

function doGetResearchData()
{
	logger.log('info', 'Querying research data...');
	
	var query = ''; //Need only insert the most recent which does not existed yet

	var alreadyExistedID = -1;

	//pgPool.doQuery("select id from research ORDER BY id DESC LIMIT 1", [], function (err, result)
	return pgPool.pgb.connect(pgPool.pgConfig).then(function (connection) {
		pgcnn = connection;     

		return pgcnn.client.query("select id from research ORDER BY id DESC LIMIT 1").then(function (result) {
			if (result.rowCount > 0)
				alreadyExistedID = result.rows[0].id;
			else
				alreadyExistedID = 0;

			console.log(alreadyExistedID);
            
            /*
			delay(100).then(() => {
				return mssqlPool.request().query('select * from research where ID > ' +alreadyExistedID)
				.then(result => {
					console.log("After fetching data from SQL Server");
					console.log(result.rowsAffected[0]);
					if (result.rowsAffected[0] > 0)
					{
						console.log("Updating to PG");
						var dt, content, promiseArr = [];
						//for (var i = 0; i < result.recordset.length; i++)
						result.recordset.forEach(function (row) {
							var fileName = row.FILE_NAME.toString().split("|");
							//var tmp = sanitizeHtml(row.RESEARCH_CONTENT, {
							var tmp = row.RESEARCH_CONTENT;
							
							var dt = row.NEWS_DATE;
							dt = GetDateOnly(dt);
							
							var notes = row.NOTES;
							
							tmp = tmp.replace(/'/g, "''");
							notes = notes.replace(/'/g, "''");
							notes = notes.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
							notes = notes.replace(/\n/g, "<br />");

							fileName[1] = fileName[1] +".pdf";
							
							//Write pdf file temporarily			    	
							fileName[1] = "./pdf/" +fileName[1];
							//logger.log("info", "Processing " +fileName[1]);			    	
							
							fsx.outputFile(fileName[1], row.FILE, "binary").then(() => {
								logger.log("info", fileName[1] +" was saved!");
							});

							//Only get the difference
							query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','" +notes +"','" +tmp +"','" +dt +"')";
							promiseArr.push(pgcnn.client.query(query));
						});

						return Promise.all(promiseArr);
					}
					else {
						logger.log("No data found.. Bailing out");
						Promise.resolve(-1);
					}
				}).catch(err => {
					console.log("Error: " +err);
				});	
            });
            */
            //Query research data from mssql 
		sql.connect(MSSQLCfg, err => {
		    const request = new sql.Request();
			request.stream = true; // You can set streaming differently for each request 
			console.log("Before query");
		    request.query('select * from research where ID > ' +alreadyExistedID); // or request.execute(procedure) 
		    //request.query('select * from research_dump');
			console.log("After query");
		    request.on('recordset', columns => {
		    	// Emitted once for each recordset in a query
			    request.on('row', row => {
			    	var fileName = row.FILE_NAME.toString().split("|");
			    	//var tmp = sanitizeHtml(row.RESEARCH_CONTENT, {
					var tmp = row.RESEARCH_CONTENT;
			    	
			    	var dt = row.NEWS_DATE;
			    	dt = GetDateOnly(dt);
			    	
					var notes = row.NOTES;
					
					notes = notes.replace(/'/g, "''");
					notes = notes.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
					notes = notes.replace(/\n/g, "<br />");

			    	tmp = tmp.replace(/'/g, "''");
			    	//tmp = tmp.replace("\'.", "\'\'.", "g");	

			    	fileName[1] = fileName[1] +".pdf";

			    	//Only get the difference
			    	query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','" +notes +"','" +tmp +"','" +dt +"')";
			    	//query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','','" +tmp +"','" +dt +"')";

			    	//Write pdf file temporarily			    	
			    	fileName[1] = "./pdf/" +fileName[1];
			    	//logger.log("info", "Processing " +fileName[1]);
			    	
			    	fs.writeFile(fileName[1], row.FILE, "binary" ,function(err) 
			    	{
					    if(err) {					 
					        console.log(err);
					    } else {
					        logger.log("info", fileName[1] +" was saved!");
					    }
					});
					
			    	//PG Query			    	
				    pgPool.doQuery(query, [], function (err, result)
				    {   
				      if (err)
				      {
				        logger.log('error', err);				        
				        console.log(fileName[1]);				        
				        return;
				      }
				    });				    				    
			    })
			 
			    request.on('error', err => {			        
			        logger.log('error', err);
			    })
			 
			    request.on('done', result => {			        
			        logger.log('info', "DONE!");
			    })
			})
		 
			sql.on('error', err => {			    
			    logger.log('sql error', err);
			})
		});
		});
	});

		/*
      	//Query research data from mssql 
		sql.connect(MSSQLCfg, err => {
		    const request = new sql.Request();
		    request.stream = true; // You can set streaming differently for each request 
		    request.query('select * from research where ID > ' +alreadyExistedID); // or request.execute(procedure) 
		    //request.query('select * from research_dump');
		 
		    request.on('recordset', columns => {
		    	// Emitted once for each recordset in a query
			    request.on('row', row => {
			    	var fileName = row.FILE_NAME.toString().split("|");
			    	//var tmp = sanitizeHtml(row.RESEARCH_CONTENT, {
					var tmp = row.RESEARCH_CONTENT;
			    	
			    	var dt = row.NEWS_DATE;
			    	dt = GetDateOnly(dt);
			    	
			    	var notes = row.NOTES;
			    	
			    	tmp = tmp.replace(/'/g, "''");
			    	//tmp = tmp.replace("\'.", "\'\'.", "g");	

			    	notes = notes.replace(/'/g, "''");
			    	//notes = notes.replace("\'.", "\'\'.", "g");	

			    	fileName[1] = fileName[1] +".pdf";

			    	//Only get the difference
			    	query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','" +notes +"','" +tmp +"','" +dt +"')";
			    	//query = "INSERT INTO public.research VALUES (" +row.ID +",'" +row.CATEGORY +"','" +row.TITLE +"','" +fileName[1] +"','','" +tmp +"','" +dt +"')";

			    	//Write pdf file temporarily			    	
			    	fileName[1] = "./pdf/" +fileName[1];
			    	//logger.log("info", "Processing " +fileName[1]);
			    	
			    	fs.writeFile(fileName[1], row.FILE, "binary" ,function(err) 
			    	{
					    if(err) {					 
					        console.log(err);
					    } else {
					        logger.log("info", fileName[1] +" was saved!");
					    }
					});
					
			    	//PG Query			    	
				    pgPool.doQuery(query, [], function (err, result)
				    {   
				      if (err)
				      {
				        logger.log('error', err);				        
				        console.log(fileName[1]);				        
				        return;
				      }
				    });				    				    
			    })
			 
			    request.on('error', err => {			        
			        logger.log('error', err);
			    })
			 
			    request.on('done', result => {			        
			        logger.log('info', "DONE!");
			    })
			})
		 
			sql.on('error', err => {			    
			    logger.log('sql error', err);
			})
		});
		*/
}

function doGetCompanyFinanceData()
{
	logger.log('info', 'Querying Company Finance data...');
	
	var query = '';

	var alreadyExistedID = -1;

	return pgPool.pgb.connect(pgPool.pgConfig).then(function (connection) {
		pgcnn = connection;
	
		return pgcnn.client.query("select id from company_finance ORDER BY id DESC LIMIT 1").then(function (result){

			if (result.rowCount > 0)
				alreadyExistedID = result.rows[0].id;
			else
				alreadyExistedID = 0;

			console.log(alreadyExistedID);

			//return mssqlPool.request().query('SELECT * FROM COMPANY_FINANCE WHERE DATE >= DATEADD(Month, DateDiff(Month, 0, DateAdd(m, -6, current_timestamp)), 0) ORDER BY DATE,ID')
			return mssqlPool.request().query('SELECT * FROM COMPANY_FINANCE ORDER BY DATE,ID')
			.then(result => {
				console.log("After fetching data from SQL Server");
				
				if (result.rowsAffected[0] > 0)
				{
					var promiseArr = [];
					result.recordset.forEach(function (row) {
						var dt = row.DATE;
						
						dt = formatDateOnlyForPG(dt);
	
						var dt2 = formatDateOnlyForPG(row.INSERT_DATE);
						
						var dt3;
						if (row.LAST_UPDATE != null)
							dt3 = formatDateOnlyForPG(row.LAST_UPDATE);
						else 
							dt3 = formatDateOnlyForPG(new Date());
						
						query = "INSERT INTO public.company_finance VALUES (" +row.ID +",'" +row.STOCK_CODE +"','" 
								+dt +"'," +row.PERIOD +"," +row.TOTAL_ASSET +"," +row.TOTAL_LIABILITY +"," +row.TOTAL_EQUITY +"," +row.REVENUE +","
								+row.GROSS_PROFIT +"," +row.OPERATING_PROFIT +"," +row.NET_PROFIT +"," +row.LAST_PRICE +"," +row.OUT_SHARE +","
								+row.BOOK_VALUE +"," + +row.EPS+"," +row.EPS_ANNUALIZED +"," +row.DER +"," +row.ROA +"," +row.ROE +"," +row.INTEREST_COV_RATIO +",'"
								+dt2 +"','" +dt3 +"') ON CONFLICT (ID) DO UPDATE SET total_asset = " +row.TOTAL_ASSET +", total_liability = " +row.TOTAL_LIABILITY +", total_equity = " +row.TOTAL_EQUITY
								+", revenue = " +row.REVENUE +", gross_profit = " +row.GROSS_PROFIT +", operating_profit = " +row.OPERATING_PROFIT +", net_profit = " +row.NET_PROFIT
								+", last_price = " +row.LAST_PRICE +", out_share = " +row.OUT_SHARE +", book_value = " +row.BOOK_VALUE +", eps = " +row.EPS + ", eps_annualized = " +row.EPS_ANNUALIZED
								+", der = " +row.DER +", roa = " +row.ROA +", roe = " +row.ROE +", interest_cov_ratio = " +row.INTEREST_COV_RATIO, ", last_update = '" + dt3 +"';";
					
						promiseArr.push(pgcnn.client.query(query));
					});

					return Promise.all(promiseArr);
				}
				else {
					logger.log("No data found.. Bailing out");
					Promise.resolve(-1);
				}
			})
		})
	});
}

function doGetIQPData()
{
	logger.log('info', 'Querying IQP data...');
	
	var query = '';

	var alreadyExistedID = -1;

	return pgPool.pgb.connect(pgPool.pgConfig).then(function (connection) {
		pgcnn = connection;
		
		//return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +aWeekAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
		return pgcnn.client.query("select message_no from nonidx_news WHERE source = 'IQP' ORDER BY message_no DESC LIMIT 1").then(function (result) {		
			/*
			if (err)
			{
				logger.log('error', err);
				return;
			}
			*/
			
			if (result.rowCount > 0)
				alreadyExistedID = result.rows[0].message_no;
			else
				alreadyExistedID = 0;
			
			console.log(alreadyExistedID);
	
			//Query iqp data from mssql
			//sql.connect(MSSQLCfg, err => {
			
			return mssqlPool.request().query("select * from NEWS_MESSAGE where SOURCE = 'IQP' AND SEQUENCE_NO > " +alreadyExistedID)
			.then(result => {
				//const request = new sql.Request();
				//request.stream = true; // You can set streaming differently for each request 
				//request.query("select * from NEWS_MESSAGE where SOURCE = 'IQP' AND SEQUENCE_NO > " +alreadyExistedID); // or request.execute(procedure)
				
				//console.log(result.recordset.length);
				console.log("After fetching data from SQL Server");
				if (result.rowsAffected[0] > 0)
				{
					console.log("Updating to PG");
					var dt, content, promiseArr = [];
					result.recordset.forEach(function (row) {
						dt = row.RECORD_TIME;
						dt = formatDateForPG(dt);
						
						content = row.CONTENT;
						content = content.replace(/'/g, "''");
						content = content.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
						content = content.replace(/\n/g, "<br />");
	
						query = "INSERT INTO public.nonidx_news VALUES (" +row.SEQUENCE_NO +",'" +row.SOURCE +"','" +row.TITLE +"',' ','"
							+content +"','" +dt +"')";
	
						promiseArr.push(pgcnn.client.query(query));
						//PG Query
						/*
						pgPool.doQuery2(query, [], function (err, result)
						{   
						  if (err)
						  {
							logger.log('error', err);				        
							return Promise.resolve(-1);
						  }
						});
						*/
					});
	
					return Promise.all(promiseArr);
				}			
			});
		});
	})
	.then(() => {
		console.log("All finished");
	});
}

//Yang ini PG dan redis specific
function doCheckAndUpdateATD()
{
	pgPool.doQuery("SELECT date FROM index_intraday ORDER BY date LIMIT 1", [], function (err, result)
    {
     	if (err)
      	{
        	logger.log('PG doCheckAndUpdateATD error', err);
        	return;
      	}

      	if (result.rowCount > 0)
		{
			//Kalau yang dari PG lebih recent dari yang ada di redis atau di redis kosong, maka insert 
			client.get("atd", function(err, reply) {				
				if (!reply)
				{
					client.set("atd", result.rows[0].date.toISOString(), redis.print);					
				}
				else 
				{
					var lastStoredATD = new Date(reply);
					var fetchedATD = new Date(result.rows[0].date);
					
					if (fetchedATD >= lastStoredATD)					
					{						
						client.set("atd", fetchedATD.toISOString(), redis.print);
					}
					else console.log("WARNING! PG ATD is less recent or equal to Redis ATD. Leaving existing value in Redis..."); //fired when holiday
				}
			});
		}
	});	
}

function doGetAndUpdateIDXStats()
{
	//Jika intraday empty, ambil dari indices_history?
	//console.log("Update IDX stats...");
		
	return mssqlPool.request().query("SELECT TOP 1 * FROM dbo.INDICES WHERE INDEX_CODE = 'COMPOSITE' ORDER BY RECORD_TIME DESC")
	.then(result => {
		
		var obj = {};
		
		if (result.rowsAffected[0] > 0)
		{
			obj.record_time = result.recordset[0].RECORD_TIME;	
			obj.sequence = result.recordset[0].SEQUENCE_NO2;
			obj.index_code = result.recordset[0].INDEX_CODE;
			obj.exchange_base_value = result.recordset[0].EXCHANGE_BASE_VALUE;
			obj.exchange_market_value = result.recordset[0].EXCHANGE_MARKET_VALUE;
			obj.previous = result.recordset[0].PREVIOUS;
			obj.opening = result.recordset[0].OPENING;
			obj.closing = result.recordset[0].CLOSING;
			obj.highest = result.recordset[0].HIGHEST;
			obj.lowest = result.recordset[0].LOWEST;
			obj.change = result.recordset[0].CHANGE;
			obj.value  = result.recordset[0].VALUE;
			obj.frequency = result.recordset[0].FREQUENCY;
			obj.volume_lot = result.recordset[0].VOLUME_LOT;
			obj.volume_bef = result.recordset[0].VOLUME_BEF;
			obj.volume_lot_bef = result.recordset[0].VOLUME_LOT_BEF;		
			
			//Kalau yang dari PG lebih recent dari yang ada di redis atau di redis kosong, maka insert 
			return client.setAsync("idx_summary", JSON.stringify(obj)).then((res) => {
				var dt = new Date().toLocaleString();
				console.log(dt +" Done updating idx...");
				return Promise.resolve();
			});
		}
		else Promise.resolve(-1);
	})
	.catch(err => {
		if (err)
			console.log(err);
		return Promise.resolve();
	});
}

function doGetAndUpdateUpDownQuote()
{
	//return sql.connect(MSSQLCfg).then( pool => {
	return mssqlPool.request().query("SELECT STOCK_CODE, closing, previous FROM [MARKET_INFO].[dbo].[STOCK_QUOTE] where board_code = 'RG'")	
	.then(result => {

		var obj = {};
		obj.up = 0;
		obj.down = 0;
		obj.unchanged = 0;

		if (result.rowsAffected[0] > 0)
		{	
			var tmp;
			for (var i = 0; i < result.rowsAffected[0]; i++)
			{				
				if (result.recordset[i].STOCK_CODE.indexOf("-") > 0)				
					continue;	
					
				if (result.recordset[i].closing == 0)
					continue;

				tmp = result.recordset[i].closing - result.recordset[i].previous;
				if (tmp > 0)				
					obj.up = obj.up + 1;
				else if (tmp < 0)
					obj.down = obj.down + 1;
				else
					obj.unchanged = obj.unchanged + 1;
			}

			//Kalau yang dari PG lebih recent dari yang ada di redis atau di redis kosong, maka insert 
			return client.setAsync("updown_summary", JSON.stringify(obj)).then((res) => {
				var dt = new Date().toLocaleString();
				console.log(dt +" Done updating updown ...");
				return Promise.resolve();
			});
		}
		else Promise.resolve(-1);		
	})
	.catch(err => {
		console.log(err);
		return Promise.resolve();
	});
}

function doUpdateIndexHistory()
{
	//1 day, 1 week, 1 month, 3 month, 6 month, ytd, 1 year, 3 year, 5 year
	
	//1 day ago
	
	//Perlu ambil yang hari sebelumnya yang terdekat!

	//pgPool.doQuery2("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = 'COMPOSITE' AND date >= '" +yesterday.format('L') +"' ORDER BY date DESC LIMIT 10")
	//var promiseArr = [];
	var currYear = moment().year();
	var yesterday = moment().subtract(1, 'days');
	var yr = yesterday.year();//.format('L');

	return pgPool.pgb.connect(pgPool.pgConfig).then(function (connection) {
		pgcnn = connection;

		return indexArr.reduce(function (pm, indexCode) {
			return pm.then(function() {
				var obj = {};
				console.log("Processing " +indexCode);
				return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +yesterday.format('L')  +"' ORDER BY date DESC LIMIT 5")
				.then(function (dayRes) {
					
					console.log("day " +indexCode);
					if (dayRes.rowCount > 0)
					{
						obj.oneDay = dayRes.rows[0].last;
						
						//1 week ago
						var aWeekAgo = moment().subtract(7, 'days');
						yr = aWeekAgo.year();
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +aWeekAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 1 day not found!");
						return Promise.resolve(-1);
					}
				})
				.then(function (weekRes) {
					
					if (weekRes.rowCount > 0)
					{					
						obj.oneWeek = weekRes.rows[0].last;
			
						//1 month ago
						var aMonthAgo = moment().subtract(1, 'months');
						yr = aMonthAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +aMonthAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 1 week not found!");
						return Promise.resolve(-1);
					}
				})	
				.then(function (monthRes) {				
					if (monthRes.rowCount > 0)
					{
						obj.oneMonth = monthRes.rows[0].last;
			
						//3 months ago
						var threeMonthAgo = moment().subtract(3, 'months');
						yr = threeMonthAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date >= '" +threeMonthAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 1 month not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (threeMonthRes) {
					if (threeMonthRes.rowCount > 0)
					{
						obj.threeMonth = threeMonthRes.rows[0].last;
			
						//6 months ago
						var sixMonthAgo = moment().subtract(6, 'months');
						yr = sixMonthAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +sixMonthAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 3 month not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (sixMonthRes) {
					if (sixMonthRes.rowCount > 0)
					{
						obj.sixMonth = sixMonthRes.rows[0].last;
			
						//YTD
						var ytdAgo = moment().startOf('year').format('L');
						
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date >= '" +ytdAgo +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 6 month not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (ytdRes) {
					if (ytdRes.rowCount > 0)
					{
						obj.ytd = ytdRes.rows[0].last;
			
						//a year ago
						var oneYearAgo = moment().subtract(1, 'year');
						yr = oneYearAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +oneYearAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" ytd not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (oneYearRes) {
					if (oneYearRes.rowCount > 0)
					{
						obj.oneYear = oneYearRes.rows[0].last;
			
						//three ago
						var threeYearAgo = moment().subtract(3, 'year');
						yr = threeYearAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +threeYearAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 1 year not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (threeYearRes) {
					if (threeYearRes.rowCount > 0)
					{
						obj.threeYear = threeYearRes.rows[0].last;			
			
						//a year months ago
						var fiveYearAgo = moment().subtract(3, 'year');
						yr = fiveYearAgo.year();
			
						return pgcnn.client.query("SELECT date, last FROM v_" +yr +"_daily_index_chart WHERE index_code = '" +indexCode +"' AND date <= '" +fiveYearAgo.format('L') +"' ORDER BY date DESC LIMIT 5");
					}
					else {
						console.log(indexCode +" 3 year not found");
						return Promise.resolve(-1);
					}
				})
				.then(function (fiveYearRes) {
					if (fiveYearRes.rowCount > 0)
					{
						obj.fiveYear = fiveYearRes.rows[0].last;					
						return Promise.resolve();
					}
					else {
						console.log(indexCode +" 5 year not found");
						//get the nearest using >=, karena mungkin saja index tersebut belum berumur 5 tahun (walaupun kecil kemungkinannya)
						return Promise.resolve(-1);
					}
				})
				.then(() => {
					//console.log(obj);
					pgcnn.done();
					console.log("Closing pg connection...");
					return client.setAsync("performance:" +indexCode, JSON.stringify(obj)).then(function (redRes) {
						var dt = new Date().toLocaleString();
						console.log(dt +" Finished updating " +indexCode);
						//return Promise.resolve();
					});
				})
				.catch(err => {
					console.log(err);
					//return Promise.resolve();
				});
			}, Promise.resolve())
			//promiseArr.push(pm);
		}, Promise.resolve())
		.then( () => {
			console.log("All done");		
		}); //end of reduce
	});	
}

function exitHandler(options, err) {
    
    if (options.cleanup) logger.log('info', 'exitHandler() cleaning up.');
    
    if (err) console.log(err.stack);
    
    if (options.exit) process.exit();
}

if (process.argv[2] == 'schedule')
{
	logger.log('info', 'Starting job scheduler (2)...');
	var jobUpdateUD = schedule.scheduleJob('*/30 * 9-16 * * 1-5', function()
    {		
        doGetAndUpdateUpDownQuote();
    });
	
    var jobUpdateIDXStats = schedule.scheduleJob('*/10 * 8-16 * * 1-5', function() //CHANGE TO 30
    {		
        doGetAndUpdateIDXStats();
    });

	var jobUpdateIndexHistory= schedule.scheduleJob('47 8 * * 1-5', function()
    {
        doUpdateIndexHistory();
    });

	//Setiap hari jam 6 pagi sampai 18 sore, setiap menit ke 59
	//masih ada bug berkaitan dengan path penyimpanan pdf?	
    var jobResearch = schedule.scheduleJob('59 6-18 * * 1-5', function()
    {
		
		var dt = new Date();
    	console.log("Current time is : " +dt);
        logger.log('info', 'Getting Research Data...');
        return doGetResearchData().then(() => {
			logger.log('Finished updating company finance from scheduler');
		});
		
	});	

	//Setiap hari jam 6.30	
    var jobFinance = schedule.scheduleJob('30 6 * * 1-5', function()
    {
		
		var dt = new Date();
    	console.log("Current time is : " +dt);
        logger.log('info', 'Getting Company Finance Data...'); 
        return doGetCompanyFinanceData().then(() => {
			logger.log('Finished updating company finance from scheduler');
		});
		
	});	

	//Setiap hari, setiap 30 menit
	var jobNewsIQP = schedule.scheduleJob('*/30 * * * 1-7', function()
	//var jobNewsIQP = schedule.scheduleJob('*/1 * * * 1-7', function()
    {
		var dt = new Date();
    	console.log("Current time is : " +dt);
        logger.log('info', 'Getting IQP Data...');
        return doGetIQPData().then(() => {
			logger.log('Finished updating iqp from scheduler');
		});
    });

	//Setiap hari, jam 4 sampai 8 pagi tiap menit pertama	
	var jobCheckATD = schedule.scheduleJob('1 4-8 * * *', function ()
	{
		
		var dt = new Date();
    	console.log("Current time is : " +dt);
		doCheckAndUpdateATD();
		
	});	
}
else
{
	if (process.argv[2] == 'research')
	{
		logger.log('info', 'Performing direct Research Data update...');
    	return doGetResearchData().then(() => {			
			process.exit(0);
		});		
	}
	else if (process.argv[2] == 'finance')
	{
		logger.log('info', 'Performing direct Company Finance update...');
    	return doGetCompanyFinanceData().then(() => {			
			process.exit(0);
		});		
	}
	else if (process.argv[2] == 'iqp')
	{
		logger.log('info', 'Performing direct IQP update...');
    	return doGetIQPData().then(() => {			
			process.exit(0);
		});
	}
	else if (process.argv[2] == 'activeday')
	{	
		logger.log('info', 'Performing direct ATD update...');
		return doCheckAndUpdateATD().then(() => {			
			process.exit(0);
		});
	}
	else if (process.argv[2] == 'idx')
	{	
		logger.log('info', 'Performing direct IDX data update...');
		return doGetAndUpdateIDXStats().then(() => {
			process.exit(0);
		});
	}
	else if (process.argv[2] == 'updown')
	{
		logger.log('info', 'Performing direct Updown data update...');
		return doGetAndUpdateUpDownQuote().then(() => {
			process.exit(0);
		});
	}
	else if (process.argv[2] == 'indexHistory')
	{
		logger.log('info', 'Performing direct Index history data update...');
		setTimeout(function() {
			return doUpdateIndexHistory().then(() => {
				process.exit(0);
			});
		}, 2000);
	}
	else
	{
		logger.error('Invalid Argument!');
	}

	mssqlPool.close();
}

//Clean up and exit handler
process.stdin.resume();//so the program will not close instantly

//App closing
process.on('exit', exitHandler.bind(null,{cleanup:true}));

//Catches ctrl+c event
process.on('SIGINT', exitHandler.bind(null, {exit:true}));

//Uncaught exceptions
process.on('uncaughtException', exitHandler.bind(null, {exit:true}));
