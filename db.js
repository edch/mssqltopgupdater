var pg = require("pg");
var Promise = require("bluebird");

Object.keys(pg).forEach(function(key) {
    var Class = pg[key];
    if (typeof Class === "function") {
        Promise.promisifyAll(Class.prototype);
        Promise.promisifyAll(Class);
    }
});

Promise.promisifyAll(pg);

var pgConfig = {
  //user: 'datafeed', //env var: PGUSER
  //database: 'DATAFEED', //env var: PGDATABASE
  //password: 'emptykosong', //env var: PGPASSWORD
  //host: '172.31.2.182', // Server hosting the postgres database
  user: 'pgadmin',
  password: 'emptykosong',
  host: '172.16.19.24',
  database: 'pgdb',
  port: 5432, //env var: PGPORT
  max: 12 // max number of clients in the pool  
};

pg.defaults.poolSize = 8;

//var conString = "postgres://datafeed:emptykosong@172.31.2.182/DATAFEED";
var conString = "postgres://pgadmin:emptykosong@172.16.19.24/pgdb";
var pool;
pg.defaults.connectionString = conString;

module.exports.init = function (callback) 
{
  pool = new pg.Pool(pgConfig);

  pool.on('error', function (err, client) 
  {
    console.error('Idle client error', err.message, err.stack)
  });
};

module.exports.doQuery = function (txt) 
{  
  return pool.connectAsync().then(function(connection) {
    return connection.queryAsync(txt)
      .catch((err) => console.log(err))
      .finally(() => connection.release(true))
  });
};

module.exports.pgConfig = pgConfig;

module.exports.cleanUp = function ()
{
  return pool.end().then(() => console.log("PG connection pool ended."));
};
